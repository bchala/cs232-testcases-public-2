// this example tests whether functions calls in
// if/else and while loop are analyzed correctly

class Example
{
	public static void main(String[] args)
	{
        System.out.println(new NumberGame().calc1(23));
        System.out.println(new NumberGame().calc2(46));
    }
}

class NumberGame
{
	int number1 = 90;
	int number2 = 80;
	int number3 = 30;
	int result = calc1(number1);

	int calc1(int rhs)
	{
		if (rhs > number1)
			result = sub(rhs, number1);
		else
			result = add(rhs, number1);

		return result;
	}

	int calc2(int rhs)
	{
		while(number3 > 10)
		{
			number3 = sub(number3, rhs);
		}
		return number3;
	}

	int add(int x, int y)
	{
		return x+y;
	}

	int sub(int x, int y)
	{
		return x-y;
	}

}
