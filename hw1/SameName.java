/* this example ensures that the call graph correctly determins which doB() is called,
 that the call graph construction can deal with a cycle, and that extraneous methods do not
 make it into the call graph */
class Example {
    public static void main(String[] args){
        A a = new A();
        B b = new B();
        C c = new C();
        a = b;
        a.doB();
    }
}

class A {
    public void doA(B b) {
        b.doB();
    }
    
    public void doA2 {
        
    }
}

class B extends A {
    public void doB() {
        this.doA(new B());
    }
    
}

class C extends A {
    public void doB() {
        this.doA2();
    }
}
